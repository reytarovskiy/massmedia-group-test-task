@extends('layouts.blog')
@section('content')
    <ul>
        <li><a href="{{ route('categories.index') }}">Категории</a></li>
        <li><a href="{{ route('categories.create') }}">Добавить категорию</a></li>
        <li><a href="{{ route('posts.create') }}">Добавить пост</a></li>
    </ul>
@endsection