@extends('layouts.blog')
@section('title', $post->name)

@section('content')
    <div class="col-md-6 col-md-offset-2">
        <h1 class="text-primary">{{ $post->name }}</h1>
        <form method="POST" action="{{ route('posts.update', $post) }}" enctype="multipart/form-data">
            {{ method_field('put') }}
            @include('partials.post.createOrUpdateForm', compact('categories'))
        </form>
    </div>
@endsection