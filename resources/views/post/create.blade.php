@extends('layouts.blog')
@section('title', 'Новый пост')

@section('content')
    <div class="col-md-6 col-md-offset-2">
        <h1 class="text-primary">Новый пост</h1>
        <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
            @include('partials.post.createOrUpdateForm', compact('categories'))
        </form>
    </div>
@endsection