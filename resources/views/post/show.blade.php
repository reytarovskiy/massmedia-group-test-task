@extends('layouts.blog')
@section('title', $post->name)

@section('css')
    <link rel="stylesheet" href="{{ asset('css/post/show.css') }}">
@endsection
@section('content')
    <div class="post">
        <h1>{{ $post->name }}</h1>
        <p>
            {!! $post->content  !!}
        </p>
        @if (!is_null($post->filename))
            <a href="{{ asset('storage/' . $post->filename) }}">Файл</a>
        @else
            <p class="text-danger">Файл не прикреплен</p>
        @endif
    </div>
@endsection
@section('sidebar')
    <a href=""></a>
    @include('partials.comments', ['url' => route('posts.comment', $post), 'comments' => $post->comments])
@endsection