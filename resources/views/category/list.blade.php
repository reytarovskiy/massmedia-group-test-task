@extends('layouts.blog')
@section('title', 'Категории')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/category/list.css') }}">
@endsection
@section('sidebar')
    <a class="btn btn-default" href="{{ route('categories.create') }}">
        <i class="glyphicon glyphicon-plus"></i>
        Добавить категорию
    </a>
@endsection
@section('content')
    <div class="categories row">
        @foreach($categories as $category)
            <div class="col-md-4">
                <div class="category">
                    <h2 class="category-name">
                        <a href="{{ route('categories.show', [$category]) }}">
                            {{ $category->name }} №{{ $category->id }}
                        </a>
                    </h2>
                    <p class="category-description">
                        {{ $category->description }}
                    </p>
                    <div class="row buttons">
                        <form action="{{ route('categories.destroy', $category) }}" method="post" class="btn-group">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <a class="btn btn-default text-warning" href="{{ route('categories.edit', $category) }}"><i class="glyphicon glyphicon-pencil"></i></a>
                            <button type="submit" class="btn btn-default text-danger">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection