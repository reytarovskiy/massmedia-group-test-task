@extends('layouts.blog')
@section('title', 'Изменение категории')

@section('content')
    <div class="col-md-6 col-md-offset-2">
        <h1 class="text-primary">Изменение категории</h1>
        <form method="POST" action="{{ route('categories.update', $category) }}">
            {{ method_field('put') }}
            @include('partials.category.createOrUpdateForm', compact('category'))
        </form>
    </div>
@endsection