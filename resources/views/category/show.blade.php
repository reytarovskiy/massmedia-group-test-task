@extends('layouts.blog')
@section('title', $category->name)
@section('css')
    <link rel="stylesheet" href="{{ asset('css/category/show.css') }}">
@endsection
@section('content')
    <h1>Посты категории: <span class="text-primary">{{ $category->name }}</span></h1>
    <div class="row">
        @foreach($category->posts as $post)
            <div class="col-md-4">
                <div class="post">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{ route('posts.show', $post) }}">
                                <span class="post-name">{{ $post->name }}</span>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <form action="{{ route('posts.destroy', $post) }}" method="post" class="btn-group">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <a class="btn btn-sm btn-default text-warning"
                                   href="{{ route('posts.edit', $post) }}"><i
                                            class="glyphicon glyphicon-pencil"></i></a>
                                <button type="submit" class="btn btn-sm btn-default text-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('sidebar')
    <a class="btn btn-default" href="{{ route('posts.create') }}">
        <i class="glyphicon glyphicon-plus"></i>
        Добавить пост
    </a>
    @include('partials.comments', ['url' => route('categories.comment', $category), 'comments' => $category->comments])
@endsection