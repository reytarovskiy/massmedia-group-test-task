@extends('layouts.blog')
@section('title', 'Новая категория')

@section('content')
    <div class="col-md-6 col-md-offset-2">
        <h1 class="text-primary">Новая категория</h1>
        <form method="POST" action="{{ route('categories.store') }}">
            @include('partials.category.createOrUpdateForm')
        </form>
    </div>
@endsection