@section('css')
    <link rel="stylesheet" href="{{ asset('css/comments.css') }}">
@append
@section('js')
    <script>
        var saveCommentUrl = '{{ $url }}';
    </script>
    <script src="{{ asset('js/category/comments.js') }}"></script>
@append
<div class="comments">
    <h4>Комментарии:</h4>
    <div class="row">
        <input placeholder="Автор" class="form-control" type="text" id="author">
    </div>
    <div class="row">
        <textarea placeholder="Комментарий" class="form-control" id="comment" cols="30" rows="5"></textarea>
    </div>
    <div class="row">
        <button class="btn btn-block btn-success" id="add-comment">Добавить</button>
    </div>
    <div class="comments-list row">
        @foreach($comments as $comment)
            <div class="comment">
                <p class="text-primary">{{ $comment->author }}:</p>
                <p>{{ $comment->content }}</p>
            </div>
        @endforeach
    </div>
</div>
