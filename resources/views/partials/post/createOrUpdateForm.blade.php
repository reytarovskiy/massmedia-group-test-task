{{ csrf_field() }}
<div class="form-group @if($errors->has('name')){{ 'has-error' }}@endif">
    <label for="name">Название</label>
    <input class="form-control" id="name" type="text" maxlength="255" name="name" value="{{ $post->name ?? '' }}">
    <ul class="text-danger">
        @foreach($errors->get('name') as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<div class="form-group @if($errors->has('content')){{ 'has-error' }}@endif">
    <label for="content">Описание</label>
    <textarea class="form-control" name="content" id="content" cols="30" rows="5" maxlength="255">
        {{ $post->content ?? '' }}
    </textarea>
    <ul class="text-danger">
        @foreach($errors->get('content') as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<div class="form-group @if($errors->has('category_id')){{ 'has-error' }}@endif">
    <label for="category">Категория</label>
    <select class="form-control" name="category_id" id="category">
        @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }} №{{ $category->id }}</option>
        @endforeach
        <option value="1000">llalala</option>
    </select>
    <ul class="text-danger">
        @foreach($errors->get('category_id') as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<div class="form-group @if($errors->has('file')){{ 'has-error' }}@endif">
    <label for="file">Файл</label>
    <input type="file" name="file" id="file">
    <ul class="text-danger">
        @foreach($errors->get('file') as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<button class="btn btn-block btn-success">Сохранить</button>
<script>
    $(document).ready(function() {
         $('#content').summernote({
             height: 200
         });
    });
</script>