{{ csrf_field() }}
<div class="form-group">
    <label for="name">Название</label>
    <input class="form-control" id="name" type="text" maxlength="255" name="name" value="{{ $category->name ?? '' }}">
</div>
<div class="form-group">
    <label for="description">Описание</label>
    <textarea class="form-control" name="description" id="description" cols="30" rows="5" maxlength="255">
        {{ $category->description ?? '' }}
    </textarea>
</div>
<button class="btn btn-block btn-success">Сохранить</button>