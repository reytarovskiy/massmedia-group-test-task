$(document).ready(function() {
    $('#add-comment').click(function () {
        var author = $('#author').val();
        var comment = $('#comment').val();
        $.ajax({
            url: saveCommentUrl,
            type: 'POST',
            data: {author: author, content: comment},
            dataType: 'json',
            success: function (response) {
                addComment(author, comment);
                $('#comment').val('');
                $('#author').val('');
            },
            error: function (error) {
                var errors = error.responseJSON.errors;
                console.log(errors);
            }
        });
    });

    function addComment(author, comment) {
        var commentElement = document.createElement('div');
        $(commentElement).addClass('comment');

        var authorElement = document.createElement('p');
        $(authorElement).addClass('text-primary').html(author);
        $(commentElement).append(authorElement);

        var contentElement = document.createElement('p');
        $(contentElement).html(comment);
        $(commentElement).append(contentElement);

        $('.comments-list').prepend(commentElement);
    }
});