<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::resource('categories', 'CategoryController');
Route::post('categories/{category}/comment', 'CategoryController@comment')
    ->name('categories.comment');

Route::resource('posts', 'PostController')->except(['index']);
Route::post('posts/{post}/comment', 'PostController@comment')
    ->name('posts.comment');