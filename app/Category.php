<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Category extends Model
{
    protected $fillable = ['name', 'description'];
    public $timestamps = false;

    public function getNameAttribute($name)
    {
        return $name ?? 'Имя не задано';
    }

    public function getDescriptionAttribute($description)
    {
        return $description ?? 'Описание не задано';
    }

    /**
     * @return Category[]|Collection
     */
    public static function list() : Collection
    {
        return self::all();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')
            ->orderBy('created_at', 'desc');
    }
}
