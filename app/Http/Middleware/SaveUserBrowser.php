<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use WhichBrowser\Parser;

class SaveUserBrowser
{
    protected $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    public function handle(Request $request, Closure $next)
    {
        $this->parser->analyse(getallheaders());

        $user = User::firstOrNew(['ip' => $request->ip()]);
        $user->browser = $this->parser->browser->name;
        $user->save();

        return $next($request);
    }
}
