<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::list();

        return view('category.list', compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $category = Category::create($request->all());

        return redirect()->route('categories.show', [$category]);
    }

    public function show(Category $category)
    {
        $category->load(['posts', 'comments']);

        return view('category.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('categories.show', $category);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('categories.index');
    }

    public function comment(CommentRequest $request, Category $category)
    {
        $comment = new Comment($request->all());
        $category->comments()->save($comment);

        return response()->json('', Response::HTTP_CREATED);
    }
}
