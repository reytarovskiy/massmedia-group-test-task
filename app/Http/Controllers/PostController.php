<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    public function create()
    {
        $categories = Category::list();

        return view('post.create', compact('categories'));
    }

    public function store(PostRequest $request)
    {
        $post = new Post($request->all());

        if ($request->hasFile('file')) {
            $post->attachFile($request->file('file'));
        }

        $post->save();

        return redirect()->route('posts.show', $post);
    }

    public function update(PostRequest $request, Post $post)
    {
        if ($request->hasFile('file')) {
            $post->attachFile($request->file('file'));
        }

        $post->update($request->all());

        return redirect()->route('posts.show', $post);
    }

    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }

    public function edit(Post $post)
    {
        $categories = Category::list();

        return view('post.edit', compact('post', 'categories'));
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('categories.index');
    }

    public function comment(CommentRequest $request, Post $post)
    {
        $comment = new Comment($request->all());
        $post->comments()->save($comment);

        return response()->json('', Response::HTTP_CREATED);
    }
}
