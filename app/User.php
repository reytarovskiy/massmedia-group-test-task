<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['ip', 'browser'];

    protected $primaryKey = 'ip';
}
