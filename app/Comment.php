<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'author'];
    const UPDATED_AT = null;

    public function commentable()
    {
        return $this->morphTo();
    }
}
